#!/usr/bin/python3
# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import functools
import multiprocessing
import os
import pathlib
import re
import shlex
import subprocess
import tempfile

from common import (
    add_database_argument,
    add_parallel_argument,
    connect_database,
    transaction,
)


def parse_diversions(output: bytes) -> dict[str, tuple[str, str, bool]]:
    """Parse the output of dpkg-divert --list into a mapping of diverted
    location to renamed location. Additionally every line is expected to
    be prefixed with either "install" or "upgrade" and the bool indicates
    upgrades. If the output cannot be parsed, a ValueError is raised.
    """
    diversions = {}
    for line in output.decode("utf8").splitlines():
        match = re.match(
            "^(install|upgrade) diversion of /(.*) to /(.*) by (.*)$", line
        )
        if not match:
            raise ValueError("invalid diversion line %r" % line)
        if match.group(2) in diversions:
            raise ValueError("%r is diverted multiple times" % match.group(2))
        diversions[match.group(2)] = (
            match.group(3),
            match.group(4),
            match.group(1) == "upgrade",
        )
    return diversions


def update_diversions(
    database: str, args: tuple[int, str, str, str, str, str]
) -> None:
    """Analyze a package using mmdebstrap and update the diversion table."""
    pid, name, version, suite, keyring, mirrors = args
    with  \
        tempfile.NamedTemporaryFile() as outf, \
        tempfile.NamedTemporaryFile() as hookf \
    :
        pkg_ver = shlex.quote(f"{name}={version}")
        hookf.write(
            f"""#!/bin/sh
set -e
cd "$1"
export LC_ALL=C.UTF-8
C=$(APT_CONFIG="$MMDEBSTRAP_APT_CONFIG" apt-cache show {pkg_ver} | sed -n '/^Conflicts:/p')
PD=$(APT_CONFIG="$MMDEBSTRAP_APT_CONFIG" apt-cache show {pkg_ver} | sed -n 's/^Pre-Depends://p')
if test -n "$C" -o -n "$PD"; then
    set --
    test -n "$C" && set -- "$@" "$C"
    test -n "$PD" && set -- "$@" "$PD"
    # apt-get satisfy behaves differently when not chrooted. Unknown cause.
    chroot . apt-get satisfy -y "$@"
fi
APT_CONFIG="$MMDEBSTRAP_APT_CONFIG" apt-get download {pkg_ver}
dpkg-divert --root . --list | sort >origdiversions
# cannot use dpkg --root due to
# https://lists.debian.org/debian-dpkg/2023/03/msg00003.html
chroot . dpkg --auto-deconfigure --unpack ./*.deb
dpkg-divert --root . --list | sort >unpackdiversions
diff origdiversions unpackdiversions | sed -n 's/^> /install /p' >addeddiversions
# Unpacking again triggers "preinst upgrade" than "preinst install".
chroot . dpkg --auto-deconfigure --unpack ./*.deb
dpkg-divert --root . --list | sort >upgradediversions
diff unpackdiversions upgradediversions | sed -n 's/^> /upgrade /p' >>addeddiversions
""".encode("ascii")
        )
        hookf.file.close()
        os.chmod(hookf.name, 0o755)
        returncode = subprocess.call(
            [
                "mmdebstrap",
                "--variant=apt",
                "--keyring=" + str(pathlib.Path(keyring).absolute()),
                "--hook-dir=/usr/share/mmdebstrap/hooks/maybe-merged-usr",
                "--customize-hook=" + shlex.quote(hookf.name),
                "--customize-hook=download addeddiversions "
                    + shlex.quote(outf.name),
                suite,
                "/dev/null",
                *mirrors,
            ]
        )
        diversion_content = outf.read()

    diversions = parse_diversions(diversion_content)
    with connect_database(database) as db, transaction(db) as cur:
        cur.execute("DELETE FROM diversion WHERE pid = ?;", (pid,))
        cur.executemany(
            "INSERT INTO diversion (pid, diverter, filename, target, upgrade) VALUES (?, ?, ?, ?, ?);",
            (
                (pid, diverter, filename, target, upgrade)
                for filename, (target, diverter, upgrade) in diversions.items()
            ),
        )
        if returncode != 0:
            cur.execute(
                """
UPDATE package SET unprocessed = unprocessed | 4 WHERE id = ?;
""",
                (pid,),
            )
        cur.execute(
            "UPDATE package SET unprocessed = unprocessed & ~2 WHERE id = ?;",
            (pid,),
        )


def main() -> None:
    parser = argparse.ArgumentParser()
    add_database_argument(parser)
    add_parallel_argument(parser, "150%")
    args = parser.parse_args()
    with multiprocessing.Pool(args.parallel) as pool:
        with connect_database(args.database) as db:
            releases = {
                row[0]: row[1:]
                for row in db.execute(
                    """
SELECT
    r.id,
    r.suite,
    r.keyring,
    r.mirror,
    group_concat(c.component, ' '),
    baserid
    FROM release AS r JOIN component AS c ON r.id = c.rid
    GROUP BY r.id;
"""
                ).fetchall()
            }
            mirrors = {
                rid: ["deb %s %s %s" % (mirror, suite, components)]
                for rid, (suite, _, mirror, components, _) in releases.items()
            }
            for rid, (_, _, _, _, baserid) in releases.items():
                if baserid is not None:
                    assert releases[baserid][4] is None
                    mirrors[rid] = mirrors[baserid] + mirrors[rid]
            res = db.execute(
                """
SELECT
    p.id,
    p.name,
    p.version,
    (
        SELECT r.id
            FROM release AS r
                JOIN component AS c ON r.id = c.rid
                JOIN package_membership AS m ON c.id = m.cid
            WHERE m.pid = p.id
            ORDER BY r.id DESC LIMIT 1
    )
    FROM package AS p
    WHERE p.unprocessed & 2 = 2;
"""
            ).fetchall()
            res = [
                (
                    pid,
                    name,
                    version,
                    releases[rid][0],
                    releases[rid][1],
                    mirrors[rid],
                )
                for pid, name, version, rid in res
            ]
        for _ in pool.imap_unordered(
            functools.partial(update_diversions, args.database), res
        ):
            pass


if __name__ == "__main__":
    main()
