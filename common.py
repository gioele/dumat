# SPDX-License-Identifier: GPL-3.0-or-later
"""Common functionality among the various tools."""

import argparse
import contextlib
import multiprocessing
import pathlib
import re
import sqlite3
import typing

import debian.debian_support


def connect_database(dbpath: str) -> sqlite3.Connection:
    database = sqlite3.connect(dbpath)
    database.execute("PRAGMA foreign_keys = true;")
    database.execute("PRAGMA busy_timeout = 300000;")
    return database


def existing_path(arg: str) -> pathlib.Path:
    """Interpret a str as a Path and verify that it exists."""
    ret = pathlib.Path(arg)
    if not ret.exists():
        raise ValueError(f"{arg!r} does not exist")
    return ret


def add_database_argument(parser: argparse.ArgumentParser) -> None:
    """Add a -d/--database argument to the given parser."""
    parser.add_argument(
        "-d",
        "--database",
        action="store",
        metavar="PATH",
        default="dumat.sqlite3",
        type=existing_path,
        help="location of the backing sqlite3 database",
    )


def parse_parallel(arg: str) -> int:
    """Parse a parallel argument string in a similar way that GNU parallel does
    it. This supports positive numbers, +N, -N, and N%.
    """
    if arg.startswith("+"):
        return multiprocessing.cpu_count() + int(arg[1:])
    if arg.startswith("-"):
        return multiprocessing.cpu_count() - int(arg[1:])
    if arg.endswith("%"):
        return multiprocessing.cpu_count() * int(arg[:-1]) // 100
    value = int(arg)
    if value <= 0:
        raise ValueError(f"invalid parallel value {value}")
    return value


def add_parallel_argument(
    parser: argparse.ArgumentParser, default: str = "1"
) -> None:
    """Add a -p/--parallel argument to the given parser."""
    parser.add_argument(
        "-p",
        "--parallel",
        action="store",
        type=parse_parallel,
        default=default,
        metavar="N",
        help="concurrency level of operation",
    )


T1 = typing.TypeVar("T1")
T2 = typing.TypeVar("T2")


def the(iterable: typing.Iterable[T1]) -> T1:
    """Return the only element from the iterable or raise a ValueError."""
    iterator = iter(iterable)
    try:
        result = next(iterator)
    except StopIteration:
        raise ValueError("no element in empty iterable")
    try:
        next(iterator)
    except StopIteration:
        return result
    raise ValueError("multiple elements in iterable")


def flip_dict(dct: dict[T1, T2]) -> dict[T2, set[T1]]:
    result: dict[T2, set[T1]] = {}
    for key, value in dct.items():
        result.setdefault(value, set()).add(key)
    return result


class Version(str):
    """A subclass of str that sorts like Debian versions."""

    def __lt__(self, other: str) -> bool:
        return debian.debian_support.version_compare(self, other) < 0

    def __le__(self, other: str) -> bool:
        return debian.debian_support.version_compare(self, other) <= 0

    def __ge__(self, other: str) -> bool:
        return debian.debian_support.version_compare(self, other) >= 0

    def __gt__(self, other: str) -> bool:
        return debian.debian_support.version_compare(self, other) > 0


def dealias_path(filename: str) -> typing.Optional[tuple[bool, str]]:
    """Determine whether a path is affected by /usr-merge and decompose it
    into a flag indicating whether it had /usr and the /usr-less component.
    """
    match = re.match(
        r"^/*(?:\./+)*(usr/)?((?:s?bin|lib|lib[ox]?32|lib64)(?:$|/.*))",
        filename,
    )
    if match is None:
        return None
    groups = match.groups()
    return (groups[0] is not None, groups[1])


@contextlib.contextmanager
def transaction(
    connection: sqlite3.Connection,
) -> typing.Iterator[sqlite3.Cursor]:
    """Begin an exclusive transaction and yield a cursor object. On context
    exit, dispose the cursor and commit unless there is an exception.
    """
    cur = connection.cursor()
    cur.execute("BEGIN EXCLUSIVE;")
    try:
        yield cur
    except:
        connection.rollback()
        raise
    else:
        connection.commit()
    finally:
        cur.close()
