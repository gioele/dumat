What is dumat?
==============

This is the Debian /usr-merge analysis tool. It aims to analyze some of the
problem classes identified in DEP-17 and generate reports.

What does dumat require?
========================

You need a Debian bookworm with packages `gpg gpgv mmdebstrap mount
python3-arpy python3-debian python3-debianbts python3-requests python3-yaml
sqlite3`. It needs to be able to run `mmdebstrap` (e.g. via user namespaces).
Beyond this, you need the software behind https://dedup.debian.net. It is added
as a git submodule.

How to use dumat?
=================

Create a database. This needs to be done once.

    sqlite3 dumat.db < schema.sql

Import packages from a mirror. This needs to be done after every mirror push.
It downloads a lot of packages and uses significant CPU resources. It also
grows the database beyond 1GB.

    ./import_mirror.py -d dumat.db

Identify diversions. This needs to be done after every mirror update. It
detects diversions by unpacking packages via mmdebstrap, so this also consumes
lots of bandwidth.

    ./import_diversions.py -d dumat.db

Optionally import known bugs from the Debian bug tracking system.

    ./import_bugs.py -d dumat.db

Perform the actual analysis. Any change to the database can result in a
different analysis. This outputs a yaml document on stdout. It is relatively
quick.

    ./analyze.py -d dumat.db

How to interpret the results?
=============================

 * "undeclared file conflict": This is a problem unrelated to /usr-merge. If
   you unpack both packages concurrently, dpkg errors out with an unpack error.
   This generally is a serious bug.
 * "ineffective replaces": This is DEP17-P1. A file is being moved from / to
   /usr and at the same time moved from one package to another. In this case,
   Replaces don't work. This typically is a serious bug.
 * "ineffective trigger interest": This is DEP17-P2. A package intends to
   trigger on a location and another package installs into this location with
   difference in the usr/ prefix. Therefore the trigger is not invoked even
   though it should be. The typically is a serious bug.
 * "ineffective diversion": This is DEP17-P3. A diversion and a diverted file
   differ in having a usr/ prefix or not. Therefore the file is not diverted.
   This typically is a serious bug.
 * "loss of empty directory": This is DEP17-P6. A directory is shipped empty in
   one package and a different package ships a file in the corresponding
   aliased directory. When that latter directory is removed, so is the empty
   directory. In many cases, the affected directory is not needed and can be
   removed from the package. This typically is a serious bug as it breaks
   piuparts.
 * "loss of m-a:same shared file": This is DEP17-P7. When upgrading one
   instance of the package and then removing another instance, the listed files
   will be deleted. This typically is a serious bug.
 * "risky ...": This is a hypothetical issue. If the package had its files
   moved to canonical locations, the corresponding "ineffective ..." or
   "loss of ..." issue were to happen. This is more of an advance notice than
   a real problem.
 * "moved from canonial to aliased": A package has moved a file from /usr to /.
   Such a move adds to the aliasing problems and is also forbidden by the
   moratorium. Moving the files in question back to /usr may cause more
   problems.
