#!/usr/bin/python3
# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import functools

import debianbts

from common import (
    add_database_argument,
    connect_database,
    flip_dict,
    the,
    transaction,
    Version,
)


usertags = {
    "fileconflict": "debian-qa@lists.debian.org",
    "dep17p1": "helmutg@debian.org",
    "dep17p2": "helmutg@debian.org",
    "dep17p3": "helmutg@debian.org",
    "dep17p6": "helmutg@debian.org",
    "dep17p7": "helmutg@debian.org",
}


def main() -> None:
    parser = argparse.ArgumentParser()
    add_database_argument(parser)
    args = parser.parse_args()
    with connect_database(args.database) as db, transaction(db) as cur:
        cur.execute("DELETE FROM bug;")
        for email, tags in flip_dict(usertags).items():
            bugmap = debianbts.get_usertag(email, sorted(tags))
            for tag, bugs in bugmap.items():
                for bug in debianbts.get_status(bugs):
                    sources = set()
                    foundversions = set()
                    fixedversions = set()
                    for ver in bug.found_versions:
                        try:
                            source, ver = ver.split("/")
                        except ValueError:
                            pass
                        else:
                            sources.add(source)
                        foundversions.add(Version(ver))
                    for ver in bug.fixed_versions:
                        try:
                            source, ver = ver.split("/")
                        except ValueError:
                            pass
                        else:
                            sources.add(source)
                        fixedversions.add(Version(ver))
                    if len(sources) == 1:
                        source = the(sources)
                        foundversion = min(foundversions, default=None)
                        fixedversion = max(fixedversions, default=None)
                    else:
                        source = foundversion = fixedversion = None
                    cur.execute(
                        """
INSERT INTO bug (id, category, source, foundversion, fixedversion)
    VALUES (?, ?, ?, ?, ?);
""",
                        (bug.bug_num, tag, source, foundversion, fixedversion),
                    )
                    affects = set(
                        pkg
                        for pkg in bug.package.split(",")
                        if not pkg.startswith("src:")
                    )
                    affects.update(
                        pkg
                        for pkg in bug.affects
                        if not pkg.startswith("src:")
                    )
                    cur.executemany(
                        """
INSERT INTO bug_affects (bid, package) VALUES (?, ?);
""",
                        ((bug.bug_num, pkg) for pkg in affects),
                    )


if __name__ == "__main__":
    main()
